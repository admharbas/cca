###CCA



# API usage

**Products**

List: You can get list of products by sending get request on /api/1.0/products


    {
    "page": 1,
    "total": 2,
    "all": false,
    "items": [
        {
            "id": 1,
            "name": "Product 1",
            "price": 12.32,
            "description": "Test product description",
            "vat": 3.02,
            "currency": null,
            "inStock": 3
        },
        {
            "id": 2,
            "name": "Product 2",
            "price": 42.5,
            "description": "Test product description",
            "vat": 2.88,
            "currency": null,
            "inStock": 1
        }
    ]
}
    

**Token**

Since this demo app doesn't have authentication you can aquire token to preserve session. To create a token send a post request to /api/1.0/tokens


  

    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Ijk5OGVkMjFhLTE4OWItNDdjZC04YWIzLWE5YTgxNjZlMjU2YSIsIm5iZiI6MTUyMTUzNTY0NywiZXhwIjoxNTIxNTM2ODQ3LCJpYXQiOjE1MjE1MzU2NDcsImlzcyI6Imlzc3VlciIsImF1ZCI6ImF1ZGllbmNlIn0.3_PVw9lVpTHuExIkqtrvO0fPqiZhv-oUBjfz1RdB5oLOJUc6nMfOUW-NT1sCuSfT4uGb7giRqIecCTIgcqPc-SnBDrhHFis9FtUTQUVL5g389miwXqCt4Ju76UOqARAWhKGEcjmbcnjTwMbHrjNgITtpopT6a2oCKqUvJ6k_GxtVFMFXfnzR4ssSNHaGCqiPFKoUY6KyjEB2NI9DHoOzpcApL7BYyKF5v3giV2K8M7QY9Seaz2LEq_cqJGyoCXVsJYe2g9Wc9DQqL4DagWWCOP6_9wx6O3vNS40JwlHu6eDUNIvYeWViiQ2CARbVWS00Ft-gs_rULyZCJwl6l_DLPw",
    "token_type": "bearer",
    "expires_in": 1200,
    "refresh_token": null

    

You should get something like this. For every request for basket manipulation token must be sent. You should send a token in the request header like this:

Authorization : Bearer [Your token goes here]

**Basket**

Create a basket

Send a post  request to the /api/1.0/baskets
It iwll create an empty basket for a customer.

Update a basket

Send a put request to the /api/1.0/baskets/1
The request should be sent using this format:

  

    {
	productItems: [
		{ productId: 1, quantity: 2 },
		{ productId: 2, quantity: 1 }
	]
	}
}

    

Response example: 

  

    {
    "basketNumber": "6410d31f-3ce5-490c-b80a-253931695d7d",
    "createdAt": "2018-03-20T09:27:49.162496+01:00",
    "modifiedAt": "2018-03-20T09:28:49.549513+01:00",
    "active": true,
    "totalPrice": 67.14,
    "productItems": [
        {
            "id": 1,
            "quantity": 2,
            "productId": 1,
            "productTitle": "Product 1",
            "productPrice": 12.32,
            "productCurrency": "BAM"
        },
        {
            "id": 2,
            "quantity": 1,
            "productId": 2,
            "productTitle": "Product 2",
            "productPrice": 42.5,
            "productCurrency": "BAM"
        }
    ]
}

    

List basket
Send get request /api/1.0/baskets/

Get basket
Send get request /api/1.0/baskets/1

Delete basket
Send delete request /api/1.0/baskets/1



###End