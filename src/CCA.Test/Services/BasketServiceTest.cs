﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac.Extras.Moq;
using CCA.Core.Entities;
using CCA.Infrastructure;
using CCA.Infrastructure.Mappers;
using CCA.Infrastructure.Mappers.Basket;
using CCA.Infrastructure.Models.Request.Basket;
using CCA.Infrastructure.Models.Request.ProductItem;
using CCA.Infrastructure.Models.Response.Basket;
using CCA.Infrastructure.Services;
using CCA.Infrastructure.Services.Implementation;
using CCA.Test.Helpers;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CCA.Test.Services
{
    public class BasketServiceTest
    {
        private CCAContext context;

        public BasketServiceTest()
        {
            context = TestDbContext.GetTestDbContext();
        }

        // TODO: Refactoring. Create helpers for data.

        public void ItemWithQuantityBiggerThanInStockCannotBeAdded()
        {
            var mock = AutoMock.GetLoose();
            mock.Provide(context);

            var customer = new User();
            context.Users.Add(customer);
            mock.Mock<IAuthService>().Setup(x => x.CurrentCustomer()).Returns(customer);

            IMapper<Basket, BasketResponseModel> basketModelMapper = mock.Create<BasketModelMapper>();
            mock.Provide(basketModelMapper);

            var basket = new Basket() { CustomerId = customer.Id };
            context.Baskets.Add(basket);
            var product = new Product() { InStock = 1 };
            context.Products.Add(product);
            context.SaveChanges();

            var basketService = mock.Create<BasketService>();

            var productItems = new List<ProductItemRequestModel>();
            productItems.Add(new ProductItemRequestModel() { ProductId = product.Id, Quantity = 2 });
            var result = basketService.Update(basket.Id, new BasketUpdateRequestModel()
            {
                ProductItems = productItems
            });

            Assert.True(basket.ProductItems.Count == 0);
        }

        [Fact]
        public void OneProductCannotBeAddedMoreThanOnce()
        {
            var mock = AutoMock.GetLoose();
            mock.Provide(context);

            var customer = new User();
            context.Users.Add(customer);
            mock.Mock<IAuthService>().Setup(x => x.CurrentCustomer()).Returns(customer);

            IMapper<Basket, BasketResponseModel> basketModelMapper = mock.Create<BasketModelMapper>();
            mock.Provide(basketModelMapper);

            var basket = new Basket() { CustomerId = customer.Id };
            context.Baskets.Add(basket);
            var product = new Product() { InStock = 2 };
            context.Products.Add(product);
            context.SaveChanges();

            var basketService = mock.Create<BasketService>();

            var productItems = new List<ProductItemRequestModel>();
            productItems.Add(new ProductItemRequestModel() { ProductId = product.Id, Quantity = 1 });
            productItems.Add(new ProductItemRequestModel() { ProductId = product.Id, Quantity = 1 });
            var result = basketService.Update(basket.Id, new BasketUpdateRequestModel()
            {
                ProductItems = productItems
            });

            Assert.True(basket.ProductItems.Count != 2);
        }
    }
}
