﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using CCA.Core.Entities;
using CCA.Infrastructure;
using CCA.Infrastructure.Mappers;
using CCA.Infrastructure.Mappers.Store;
using CCA.Infrastructure.Models.Request.Store;
using CCA.Infrastructure.Models.Response.Store;
using CCA.Infrastructure.Services.Implementation;
using CCA.Test.Helpers;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CCA.Test.Services
{
    public class StoreServiceTest
    {
        private CCAContext context;

        public StoreServiceTest()
        {
            context = TestDbContext.GetTestDbContext();
        }

        [Fact]
        public void GetStoresShouldReturnSomeIfThereAreAny()
        {
            var mock = AutoMock.GetLoose();
            mock.Provide(context);

            IMapper<IEnumerable<Store>, StoreListResponseModel> storeListModelMapper = mock.Create<StoreListModelMapper>();
            mock.Provide(storeListModelMapper);

            context.Stores.Add(new Store()
            {
                Id = 1,
                Name = "Test Store",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            });
            context.SaveChanges();

            var storeService = mock.Create<StoreService>();

            var result = storeService.Get(new StoreListRequestModel());
            Assert.True(result.Value.Items.Count() == 1);
        }
    }
}
