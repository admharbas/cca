﻿using System;
using CCA.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CCA.Test.Helpers
{
    public static class TestDbContext
    {
        public static CCAContext GetTestDbContext()
        {
            var options = new DbContextOptionsBuilder<CCAContext>()
                        .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;
            var context = new CCAContext(options);

            return context;
        }
    }
}
