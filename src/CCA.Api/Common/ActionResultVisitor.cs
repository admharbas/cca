﻿using System;
using CCA.Infrastructure.Services.Result;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CCA.Api.Common
{
    public class ActionResultVisitor<T> : IServiceResultVisitor<T, IActionResult>
    {
        public IActionResult VisitEntityDeleted(EntityDeletedServiceResult result)
        {
            return new OkObjectResult(result.Value);
        }

        public IActionResult VisitEntityForbidden(ForbiddenServiceResult result)
        {
            var r = new ObjectResult("Access denied.");
            r.StatusCode = StatusCodes.Status403Forbidden;
            return r;
        }

        public IActionResult VisitError(ErrorServiceResult result)
        {
            return new BadRequestObjectResult(result.Message);
        }

        public IActionResult VisitNotFound(NotFoundServiceResult result)
        {
            return new NotFoundResult();
        }

        public IActionResult VisitOk(OkServiceResult<T> result)
        {
            return new OkObjectResult(result.Value);
        }
    }
}
