﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using CCA.Infrastructure.Registration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CCA.Api.Config
{
    public class IocConfig
    {
        public static IServiceProvider ConfigureServices(
            IServiceCollection services,
            IConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<MappingModule>();
            builder.RegisterModule<ServiceModule>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            builder.Populate(services);

            var container = builder.Build();

            return new AutofacServiceProvider(container);
        }
    }
}
