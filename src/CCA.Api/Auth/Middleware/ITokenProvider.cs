﻿using System;
using CCA.Core.Entities;
using Microsoft.IdentityModel.Tokens;

namespace CCA.Api.Auth.Middleware
{
    public interface ITokenProvider
    {
        string CreateToken(string userName, DateTime expiry);
        TokenValidationParameters GetValidationParameters();
    }
}
