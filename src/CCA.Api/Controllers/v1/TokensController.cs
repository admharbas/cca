﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CCA.Api.Auth.Middleware;
using CCA.Core.Entities;
using CCA.Infrastructure;
using CCA.Infrastructure.Models.Request.Customer;
using CCA.Infrastructure.Models.Request.Token;
using CCA.Infrastructure.Services;
using CCA.Infrastructure.Services.Result;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace CCA.Api.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class TokensController : BaseController
    {
        private ICustomerService customerService;
        private ITokenService tokenService;
        private ITokenProvider tokenProvider;
        private CCAContext context;

        public TokensController(ICustomerService customerService
                                ,ITokenService tokenService
                                ,ITokenProvider tokenProvider
                                ,CCAContext ccaContext)
        {
            this.customerService = customerService;
            this.tokenService = tokenService;
            this.tokenProvider = tokenProvider;
            this.context = ccaContext;
        }

        [AllowAnonymous]
        [HttpPost("")]
        public IActionResult Create(TokenCreateRequestModel model)
        {
            var customer = customerService.Create(new CustomerCreateModel());
            int ageInMinutes = 20;

            DateTime expiry = DateTime.UtcNow.AddMinutes(ageInMinutes);

            var token = new JsonWebToken
            {
                access_token = tokenProvider.CreateToken(customer.Value.CustomerNumber, expiry),
                expires_in = ageInMinutes * 60
            };

            return Convert(new OkServiceResult<JsonWebToken>(token));
        }
    }
}
