﻿using System;
using CCA.Infrastructure.Models.Request.Store;
using CCA.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace CCA.Api.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class StoresController : BaseController
    {
        private IStoreService storeService; 
        
        public StoresController(IStoreService storeService)
        {
            this.storeService = storeService;
        }

        [HttpGet("")]
        public IActionResult Get([FromQuery] StoreListRequestModel model)
        {
            var result = storeService.Get(model);
            return Convert(result);
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int Id)
        {
            var result = storeService.Get(Id);
            return Convert(result);
        }
    }
}
