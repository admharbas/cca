﻿using System;
using CCA.Infrastructure.Models.Request.Product;
using CCA.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace CCA.Api.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class ProductsController : BaseController
    {
        private IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet("")]
        public IActionResult Get(ProductRequestListModel model)
        {
            var result = productService.Get(model);
            return Convert(result);
        }
    }
}
