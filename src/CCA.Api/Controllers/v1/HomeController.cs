﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace CCA.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/home")]
    public class HomeV1Controller : Controller
    {
        [HttpGet]
        public string Get() => "Version 1";
    }
}
