﻿using System;
using CCA.Infrastructure.Models.Request.Basket;
using CCA.Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CCA.Api.Controllers.v1
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class BasketsController : BaseController
    {
        private IBasketService basketService;

        public BasketsController(IBasketService basketService)
        {
            this.basketService = basketService;
        }

        [HttpGet("")]
        public IActionResult Get(BasketListRequestModel model)
        {
            var result = basketService.Get(model);
            return Convert(result);
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var result = basketService.Get(id);
            return Convert(result);
        }

        [HttpPost("")]
        public IActionResult Create(BasketCreateRequestModel model)
        {
            var result = basketService.Create(model);
            return Convert(result);
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(int id, [FromBody] BasketUpdateRequestModel model)
        {
            var result = basketService.Update(id, model);
            return Convert(result);
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var result = basketService.Delete(id);
            return Convert(result);
        }
    }
}
