﻿using System;
using CCA.Api.Common;
using CCA.Infrastructure.Services.Result;
using Microsoft.AspNetCore.Mvc;

namespace CCA.Api.Controllers.v1
{
    public class BaseController : Controller
    {
        public IActionResult Convert<T>(ServiceResult<T> result)
        {
            var visitor = new ActionResultVisitor<T>();
            return result.Visit(visitor);
        }
    }
}
