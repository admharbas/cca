﻿using System;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Token;
using CCA.Infrastructure.Services.Result;
using Microsoft.IdentityModel.Tokens;

namespace CCA.Infrastructure.Services
{
    public interface ITokenService : IService
    {
        ServiceResult<TokenResponseModel> Create(int customerId, DateTime expiry);
    }
}
