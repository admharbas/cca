﻿using System;
using CCA.Infrastructure.Models.Request.Product;
using CCA.Infrastructure.Models.Response.Product;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services
{
    public interface IProductService
    {
        ServiceResult<ProductListResponseModel> Get(ProductRequestListModel model);
    }
}
