﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public class OkServiceResult<T> : ServiceResult<T>
    {
        private T _value;

        public override T Value { get { return _value; } }

        public override bool IsOk { get { return true; } }

        public override string Message { get { return "OK"; } }

        public OkServiceResult(T value)
        {
            this._value = value;
        }

        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitOk(this as OkServiceResult<TIn>);
        }
	}
}
