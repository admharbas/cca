﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public class EntityDeletedServiceResult : ServiceResult
    {
        public override bool IsOk { get { return true; } }

        public EntityDeletedServiceResult()
            : base("Entity deleted successfully.")
        {
        }

        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitEntityDeleted(this);
        }
    }
}
