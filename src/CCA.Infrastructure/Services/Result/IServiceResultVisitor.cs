﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public interface IServiceResultVisitor<Tin, TRet>
    {
        TRet VisitOk(OkServiceResult<Tin> result);

        TRet VisitNotFound(NotFoundServiceResult result);

        TRet VisitEntityDeleted(EntityDeletedServiceResult result);

        TRet VisitEntityForbidden(ForbiddenServiceResult result);

        TRet VisitError(ErrorServiceResult result);
    }
}
