﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public class ForbiddenServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public ForbiddenServiceResult()
            : base("Access denied.")
        {
        }

        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitEntityForbidden(this);
        }
    }
}
