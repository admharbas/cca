﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public class NotFoundServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public NotFoundServiceResult()
            : base("Entity was not found.")
        {
        }

        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitNotFound(this);
        }
    }
}
