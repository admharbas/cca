﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public class ServiceResultWrapper<T> : ServiceResult<T>
    {
        public ServiceResult<Nothing> InnerResult { get; private set; }

        public override T Value { get { return default(T); } }

        public override bool IsOk { get { return InnerResult.IsOk; } }

        public override string Message { get { return InnerResult.Message; } }

        public ServiceResultWrapper(ServiceResult<Nothing> innerResult)
        {
            this.InnerResult = innerResult;
        }

		public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
		{
            return InnerResult.Visit(visitor);
		}
	}
}
