﻿using System;
namespace CCA.Infrastructure.Services.Result
{
    public class ServiceResultWrapperVisitor<TIn> : IServiceResultVisitor<TIn, ServiceResultWrapper<TIn>>
    {
        public ServiceResultWrapper<TIn> VisitEntityDeleted(EntityDeletedServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitEntityForbidden(ForbiddenServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitError(ErrorServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitNotFound(NotFoundServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitOk(OkServiceResult<TIn> result)
        {
            throw new InvalidOperationException();
        }
    }
}
