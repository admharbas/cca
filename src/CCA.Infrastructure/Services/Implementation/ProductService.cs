﻿using System;
using System.Linq;
using Autofac;
using CCA.Core.Entities;
using CCA.Infrastructure.Extensions;
using CCA.Infrastructure.Models.Request.Product;
using CCA.Infrastructure.Models.Response.Product;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services.Implementation
{
    public class ProductService : Service, IProductService
    {
        private CCAContext context;

        public ProductService(ILifetimeScope scope, CCAContext context)
            : base(scope)
        {
            this.context = context;
        }

        public ServiceResult<ProductListResponseModel> Get(ProductRequestListModel model)
        {
            var query = context.Products.AsQueryable();

            var total = query.Count();
            var stores = query.Paginate(model);

            var mapped = Map<Product, ProductListResponseModel>(stores);
            if (!mapped.IsOk)
                return mapped;

            mapped.Value.Page = model.Page;
            mapped.Value.Total = total;
            return mapped;
        }
    }
}
