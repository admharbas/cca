﻿using System;
using System.Collections.Generic;
using Autofac;
using CCA.Infrastructure.Mappers;
using CCA.Infrastructure.Services.Result;
using Microsoft.Extensions.DependencyInjection;

namespace CCA.Infrastructure.Services.Implementation
{
    public class Service : IService
    {
        public ILifetimeScope Scope { get; set; }

        public Service(ILifetimeScope scope)
        {
            this.Scope = scope;
        }

        public OkServiceResult<T> Ok<T>(T value)
        {
            return new OkServiceResult<T>(value);
        }

        public virtual ServiceResult<TTo> Map<TFrom, TTo>(TFrom value)
        {
            var mapper = Scope.Resolve<IMapper<TFrom, TTo>>();
            if (mapper == null)
                return null;

            return Ok(mapper.Map(value));
        }

        public ServiceResult<TTo> Map<TFrom, TTo>(IEnumerable<TFrom> values)
        {
            var mapper = Scope.Resolve<IMapper<IEnumerable<TFrom>, TTo>>();
            if (mapper == null)
                return null;

            return Ok(mapper.Map(values));
        }
    }
}
