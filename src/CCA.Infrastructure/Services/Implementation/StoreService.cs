﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CCA.Core.Entities;
using CCA.Infrastructure.Extensions;
using CCA.Infrastructure.Models.Request.Store;
using CCA.Infrastructure.Models.Response.Store;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services.Implementation
{
    public class StoreService : Service, IStoreService
    {
        private CCAContext context;

        public StoreService(ILifetimeScope scope, CCAContext context)
            : base(scope)
        {
            this.context = context;
            this.Scope = scope;
        }

        public ServiceResult<StoreListResponseModel> Get(StoreListRequestModel model)
        {
            var query = context.Stores.AsQueryable();

            if (!String.IsNullOrEmpty(model.Filter))
                query = query.Where(x => x.Name.Contains(model.Filter));

            var total = query.Count();
            var stores = query.Paginate(model);

            var mapped = Map<Store, StoreListResponseModel>(stores);
            if (!mapped.IsOk)
                return mapped;

            mapped.Value.Page = model.Page;
            mapped.Value.Total = total;
            return mapped;
        }

        public ServiceResult<StoreReponseModel> Get(int id)
        {
            var store = context.Stores.FirstOrDefault(x => x.Id == id);

            var mapped = Map<Store, StoreReponseModel>(store);
            return mapped;
        }
    }
}
