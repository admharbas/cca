﻿using System;
using System.Linq;
using Autofac;
using CCA.Core.Entities;
using CCA.Infrastructure;
using CCA.Infrastructure.Services;
using CCA.Infrastructure.Services.Implementation;
using Microsoft.AspNetCore.Http;

namespace CCA.Infrastructure.Services
{
    public class AuthService : Service, IAuthService
    {
        private CCAContext context;
        IHttpContextAccessor accessor;

        public AuthService(ILifetimeScope scope, CCAContext context, IHttpContextAccessor accessor)
            : base(scope)

        {
            this.context = context;
            this.accessor = accessor;
        }

        public User CurrentCustomer()
        {
            var userName = accessor.HttpContext.User.Identity.Name;
            return context.Users.FirstOrDefault(x => x.CustomerNumber == userName);
        }
    }
}
