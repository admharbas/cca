﻿using System;
using System.Linq;
using Autofac;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Request.Customer;
using CCA.Infrastructure.Models.Response.Customer;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services.Implementation
{
    public class CustomerService : Service, ICustomerService
    {
        private CCAContext context;

        public CustomerService(ILifetimeScope scope, CCAContext context)
            : base(scope)
        {
            this.context = context;
            this.Scope = scope;
        }

        public ServiceResult<CustomerResponseModel> Create(CustomerCreateModel model)
        {
            var customerNumber = Guid.NewGuid().ToString();
            var customer = new User()
            {
                UserName = customerNumber,
                CustomerNumber = customerNumber,
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };

            context.Users.Add(customer);
            context.SaveChanges();

            return this.Get(customer.CustomerNumber);
        }

        public ServiceResult<CustomerResponseModel> Get(string customerNumber)
        {
            var customer = context.Users.FirstOrDefault(x => x.CustomerNumber == customerNumber);
            if (customer == null)
                return new NotFoundServiceResult();

            var mapped = Map<User, CustomerResponseModel>(customer);
            return mapped;
        }
    }
}
