﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using Autofac;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Token;
using CCA.Infrastructure.Services.Result;
using Microsoft.IdentityModel.Tokens;

namespace CCA.Infrastructure.Services.Implementation
{
    public class TokenService : Service, ITokenService
    {
        private CCAContext context;

        public TokenService(ILifetimeScope scope, CCAContext context)
            : base(scope)
        {
            this.context = context;
        }

        public ServiceResult<TokenResponseModel> Create(int customerId, DateTime expiry)
        {
            var customer = context.Users.FirstOrDefault(x => x.Id == customerId);
            if (customer == null)
                return new NotFoundServiceResult();

            var expiryDate = DateTime.Now.AddDays(3);

            var token = new Token()
            {
                CustomerId = customer.Id,
                OwnerCustomerNumber = customer.CustomerNumber,
                ExpiryDate = expiryDate
            };

            context.Tokens.Add(token);
            var mapped = Map<Token, TokenResponseModel>(token);

            return mapped;
        }
    }
}
