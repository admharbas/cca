﻿using System;
using System.Linq;
using Autofac;
using CCA.Core.Entities;
using CCA.Infrastructure.Extensions;
using CCA.Infrastructure.Models.Request.Basket;
using CCA.Infrastructure.Models.Response.Basket;
using CCA.Infrastructure.Services.Result;
using Microsoft.EntityFrameworkCore;

namespace CCA.Infrastructure.Services.Implementation
{
    public class BasketService : Service, IBasketService
    {
        private CCAContext context;
        private IAuthService authService;

        public BasketService(ILifetimeScope scope, CCAContext context, IAuthService authService)
            : base(scope)
        {
            this.context = context;
            this.authService = authService;
        }

        public ServiceResult<BasketListResponseModel> Get(BasketListRequestModel model)
        {
            var currentCustomer = authService.CurrentCustomer();

            if (currentCustomer == null)
                return new ForbiddenServiceResult();

            var query = context.Baskets.Where(x => x.CustomerId == currentCustomer.Id).AsQueryable();

            var total = query.Count();
            var baskets = query.Paginate(model);

            var mapped = Map<Basket, BasketListResponseModel>(baskets);
            if (!mapped.IsOk)
                return mapped;

            mapped.Value.Page = model.Page;
            mapped.Value.Total = total;
            return mapped;
        }

        public ServiceResult<BasketResponseModel> Get(int id)
        {
            var currentCustomer = authService.CurrentCustomer();

            if (currentCustomer == null)
                return new ForbiddenServiceResult();

            var basket = context.Baskets
                                .Include(x => x.ProductItems)
                                .ThenInclude(x => x.Product)
                                .FirstOrDefault(x => x.Id == id && x.CustomerId == currentCustomer.Id);
            
            if (basket == null)
                return new NotFoundServiceResult();

            var mapped = Map<Basket, BasketResponseModel>(basket);
            return mapped;
        }

        public ServiceResult<BasketResponseModel> Create(BasketCreateRequestModel model)
        {
            var currentCustomer = authService.CurrentCustomer();

            if (currentCustomer == null)
                return new ForbiddenServiceResult();

            var basket = new Basket()
            {
                Active = true,
                BasketNumber = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                CustomerId = currentCustomer.Id
            };

            context.Baskets.Add(basket);
            context.SaveChanges();

            return this.Get(basket.Id);
        }

        // TODO: Method refactoring
        public ServiceResult<BasketResponseModel> Update(int id, BasketUpdateRequestModel model)
        {
            var currentCustomer = authService.CurrentCustomer();

            if (currentCustomer == null)
                return new ForbiddenServiceResult();
            
            var basket = context.Baskets
                                .Include(x => x.ProductItems)
                                .ThenInclude(x => x.Product)
                                .FirstOrDefault(x => x.Id == id && x.CustomerId == currentCustomer.Id);

            if (basket == null)
                return new NotFoundServiceResult();

            var duplicateProducts = model.ProductItems
                                         .GroupBy(x => x.ProductId)
                                         .Where(x => x.Count() > 1);
            
            if(duplicateProducts.Count() != 0)
            {
                return new ErrorServiceResult("Duplicate products in the request");
            }

            // remove product items from ocntext if they doesn't exist in model
            foreach(var productItem in basket.ProductItems)
            {
                var item = model.ProductItems.FirstOrDefault(x => x.ProductId == productItem.ProductId);
                if(item == null) {
                    context.ProductItems.Remove(productItem);
                }
            }

            // add and update product items in context from model
            foreach(var productItem in model.ProductItems)
            {
                var dbProduct = context.Products.FirstOrDefault(x => x.Id == productItem.ProductId);
                if (dbProduct == null)
                    return new ErrorServiceResult(String.Format("Product with id: {0} doesn't exist", productItem.ProductId));
                if (productItem.Quantity < 1 || dbProduct.InStock < productItem.Quantity)
                    return new ErrorServiceResult(String.Format("Can't choose that quantity for product with id: {0}", productItem.ProductId));
                
                var oldProductItem = basket.ProductItems
                                           .FirstOrDefault(x => x.ProductId == productItem.ProductId);

                // if product item doesn't exist in context add them
                if(oldProductItem == null)
                {
                    context.ProductItems.Add(new ProductItem()
                    {
                        BasketId = basket.Id,
                        CreatedAt = DateTime.Now,
                        ModifiedAt = DateTime.Now,
                        ProductId = productItem.ProductId,
                        Quantity = productItem.Quantity
                    });

                    continue;
                }

                // if product item exist in context but there is a difference in quantity, update
                if(productItem.Quantity != oldProductItem.Quantity)
                {
                    oldProductItem.ModifiedAt = DateTime.Now;
                    oldProductItem.Quantity = productItem.Quantity;
                }
            }

            basket.ModifiedAt = DateTime.Now;

            context.SaveChanges();
            UpdateBasketPrice(basket.Id);

            return this.Get(basket.Id);
        }

        public ServiceResult Delete(int id)
        {
            var currentCustomer = authService.CurrentCustomer();

            if (currentCustomer == null)
                return new ForbiddenServiceResult();
            
            var basket = context.Baskets.FirstOrDefault(x => x.Id == id);
            if (basket == null)
                return new NotFoundServiceResult();

            context.Baskets.Remove(basket);
            context.SaveChanges();

            return new EntityDeletedServiceResult();
        }

        private void UpdateBasketPrice(int id)
        {
            var basket = context.Baskets
                                .Include(x => x.ProductItems)
                                .ThenInclude(x => x.Product)
                                .FirstOrDefault(x => x.Id == id);
            
            if (basket == null)
                return;

            var sum = basket.ProductItems.Sum(x => x.Product.Price * x.Quantity);
            basket.TotalPrice = sum;
            context.SaveChanges();
        }
    }
}
