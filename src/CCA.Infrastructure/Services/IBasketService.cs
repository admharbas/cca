﻿using System;
using CCA.Infrastructure.Models.Request.Basket;
using CCA.Infrastructure.Models.Response.Basket;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services
{
    public interface IBasketService
    {
        ServiceResult<BasketListResponseModel> Get(BasketListRequestModel model);
        ServiceResult<BasketResponseModel> Get(int id);
        ServiceResult<BasketResponseModel> Create(BasketCreateRequestModel model);
        ServiceResult<BasketResponseModel> Update(int id, BasketUpdateRequestModel model);
        ServiceResult Delete(int id);
    }
}
