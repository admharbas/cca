﻿using System;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Request.Customer;
using CCA.Infrastructure.Models.Response.Customer;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services
{
    public interface ICustomerService
    {
        ServiceResult<CustomerResponseModel> Get(string customerNumber);
        ServiceResult<CustomerResponseModel> Create(CustomerCreateModel model);
    }
}
