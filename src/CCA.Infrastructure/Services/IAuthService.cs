﻿using System;
using CCA.Core.Entities;

namespace CCA.Infrastructure.Services
{
    public interface IAuthService
    {
        User CurrentCustomer();
    }
}
