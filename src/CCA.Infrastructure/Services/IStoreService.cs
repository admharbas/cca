﻿using System;
using CCA.Infrastructure.Models.Request.Store;
using CCA.Infrastructure.Models.Response.Store;
using CCA.Infrastructure.Services.Result;

namespace CCA.Infrastructure.Services
{
    public interface IStoreService
    {
        ServiceResult<StoreListResponseModel> Get(StoreListRequestModel model);
        ServiceResult<StoreReponseModel> Get(int id);
    }
}
