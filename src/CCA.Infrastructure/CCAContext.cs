﻿using System;
using CCA.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CCA.Infrastructure
{
    public class CCAContext : DbContext
    {
        public CCAContext(DbContextOptions<CCAContext> options)
            : base(options)
        {
        }

        public DbSet<Store> Stores { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductItem> ProductItems { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductItem>()
                        .HasOne(s => s.Product)
                        .WithMany(s => s.ProductItems)
                        .HasForeignKey(s => s.ProductId);

            modelBuilder.Entity<ProductItem>()
                        .HasOne(s => s.Basket)
                        .WithMany(s => s.ProductItems)
                        .HasForeignKey(s => s.BasketId);

            modelBuilder.Entity<ProductItem>()
                        .HasOne(s => s.Order)
                        .WithMany(s => s.ProductItems)
                        .HasForeignKey(s => s.OrderId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
