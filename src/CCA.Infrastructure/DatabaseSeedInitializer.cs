﻿using System;
using System.Threading.Tasks;
using System.Linq;
using CCA.Core.Entities;

namespace CCA.Infrastructure
{
    public class DatabaseSeedInitializer
    {
        public async static Task Seed(CCAContext context)
        {
            if (!context.Stores.Any())
            {
                context.Stores.Add(new Store()
                {
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    Name = "Test Store",
                    PhoneNumber = String.Empty
                });

                await context.SaveChangesAsync();
            }

            if (!context.Products.Any())
            {
                context.Products.Add(new Product()
                {
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    Title = "Product 1",
                    Active = true,
                    Currency = "BAM",
                    Description = "Test product description",
                    InStock = 3,
                    Price = 12.32,
                    StoreId = 1,
                    VAT = 3.02
                });

                context.Products.Add(new Product()
                {
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    Title = "Product 2",
                    Active = true,
                    Currency = "BAM",
                    Description = "Test product description",
                    InStock = 1,
                    Price = 42.50,
                    StoreId = 1,
                    VAT = 2.88
                });

                await context.SaveChangesAsync();
            }
        }
    }
}
