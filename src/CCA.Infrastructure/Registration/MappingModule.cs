﻿using System;
using Autofac;
using CCA.Infrastructure.Mappers;

namespace CCA.Infrastructure.Registration
{
    public class MappingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
                   .AsClosedTypesOf(typeof(IMapper<,>));
        }
    }
}
