﻿using System;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Product;

namespace CCA.Infrastructure.Mappers.Product
{
    public class ProductListModelItemMapper : IMapper<CCA.Core.Entities.Product, ProductListModelItem>
    {
        public ProductListModelItem Map(Core.Entities.Product value)
        {
            return new ProductListModelItem()
            {
                Id = value.Id,
                Name = value.Title,
                Description = value.Description,
                VAT = value.VAT,
                Price = value.Price,
                InStock = value.InStock,
                Currency = value.Currency
            };
        }
    }
}
