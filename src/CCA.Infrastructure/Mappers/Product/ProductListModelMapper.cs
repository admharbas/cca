﻿using System;
using System.Collections.Generic;
using System.Linq;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Product;

namespace CCA.Infrastructure.Mappers.Product
{
    public class ProductListModelMapper : IMapper<IEnumerable<CCA.Core.Entities.Product>, ProductListResponseModel>
    {
        public IMapper<CCA.Core.Entities.Product, ProductListModelItem> ItemMapper { get; set; }

        public ProductListModelMapper(IMapper<CCA.Core.Entities.Product, ProductListModelItem> itemMapper)
        {
            this.ItemMapper = itemMapper;
        }

        public ProductListResponseModel Map(IEnumerable<Core.Entities.Product> value)
        {
            var items = value.Select(x => ItemMapper.Map(x))
                              .ToList();

            return new ProductListResponseModel()
            {
                Items = items,
                Total = items.Count
            };
        }
    }
}
