﻿using System;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Customer;
using CCA.Infrastructure.Models.Response.Token;

namespace CCA.Infrastructure.Mappers.Token
{
    public class TokenModelMapper : IMapper<CCA.Core.Entities.Token, TokenResponseModel>
    { 
        public TokenResponseModel Map(Core.Entities.Token value)
        {
            return new TokenResponseModel()
            {
                Id = value.Id,
                ExpiryDate = value.ExpiryDate
            };
        }
    }
}
