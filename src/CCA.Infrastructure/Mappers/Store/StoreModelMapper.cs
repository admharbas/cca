﻿using System;
using CCA.Infrastructure.Models.Response.Store;

namespace CCA.Infrastructure.Mappers.Store
{
    public class StoreModelMapper : IMapper<CCA.Core.Entities.Store, StoreReponseModel>
    {
        public StoreReponseModel Map(CCA.Core.Entities.Store value)
        {
            return new StoreReponseModel()
            {
                Id = value.Id,
                Name = value.Name,
                PhoneNumber = value.PhoneNumber
            };
        }
    }
}
