﻿using System;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Store;

namespace CCA.Infrastructure.Mappers.Store
{
    public class StoreListModelItemMapper : IMapper<CCA.Core.Entities.Store, StoreListModelItem>
    {
        public StoreListModelItem Map(Core.Entities.Store value)
        {
            return new StoreListModelItem()
            {
                Id = value.Id,
                Name = value.Name
            };
        }
    }
}
