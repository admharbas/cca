﻿using System;
using System.Collections.Generic;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Store;
using System.Linq;

namespace CCA.Infrastructure.Mappers.Store
{
    public class StoreListModelMapper : IMapper<IEnumerable<CCA.Core.Entities.Store>, StoreListResponseModel>
    {
        public IMapper<CCA.Core.Entities.Store, StoreListModelItem> ItemMapper { get; set; }

        public StoreListModelMapper(IMapper<CCA.Core.Entities.Store, StoreListModelItem> itemMapper)
        {
            this.ItemMapper = itemMapper;
        }

        public StoreListResponseModel Map(IEnumerable<Core.Entities.Store> stores)
        {
            var items = stores.Select(x => ItemMapper.Map(x))
                              .ToList();

            return new StoreListResponseModel()
            {
                Items = items,
                Total = items.Count
            };
        }
    }
}
