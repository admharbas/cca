﻿using System;
namespace CCA.Infrastructure.Mappers
{
    public interface IMapper<TFrom, TTo>
    {
        TTo Map(TFrom value);
    }
}
