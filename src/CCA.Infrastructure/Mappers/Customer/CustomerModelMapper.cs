﻿using System;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Customer;

namespace CCA.Infrastructure.Mappers.Customer
{
    public class CustomerModelMapper : IMapper<CCA.Core.Entities.User, CustomerResponseModel>
    {

        public CustomerResponseModel Map(Core.Entities.User value)
        {
            return new CustomerResponseModel()
            {
                Id = value.Id,
                FirstName = value.FirstName,
                LastName = value.LastName,
                Address = value.Address,
                PhoneNumber = value.PhoneNumber,
                CustomerNumber = value.CustomerNumber
            };
        }
    }
}
