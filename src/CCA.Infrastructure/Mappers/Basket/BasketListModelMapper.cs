﻿using System;
using System.Collections.Generic;
using System.Linq;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Basket;

namespace CCA.Infrastructure.Mappers.Basket
{
    public class BasketListModelMapper : IMapper<IEnumerable<CCA.Core.Entities.Basket>, BasketListResponseModel>
    {
        public IMapper<CCA.Core.Entities.Basket, BasketResponseModel> ItemMapper { get; set; }

        public BasketListModelMapper(IMapper<CCA.Core.Entities.Basket, BasketResponseModel> itemMapper)
        {
            this.ItemMapper = itemMapper;
        }

        public BasketListResponseModel Map(IEnumerable<Core.Entities.Basket> baskets)
        {
            var items = baskets.Select(x => ItemMapper.Map(x))
                               .ToList();

            return new BasketListResponseModel()
            {
                Items = items,
                Total = items.Count
            };
        }
    }
}
