﻿using System;
using System.Linq;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.Basket;
using CCA.Infrastructure.Models.Response.ProductItem;

namespace CCA.Infrastructure.Mappers.Basket
{
    public class BasketModelMapper : IMapper<CCA.Core.Entities.Basket, BasketResponseModel>
    {
        public BasketResponseModel Map(Core.Entities.Basket value)
        {
            return new BasketResponseModel()
            {
                Active = value.Active,
                BasketNumber = value.BasketNumber,
                CreatedAt = value.CreatedAt,
                ModifiedAt = value.ModifiedAt,
                TotalPrice = value.TotalPrice,
                ProductItems = value.ProductItems.Select(x => new ProductItemResponseModel(){
                    Id = x.Id,
                    ProductId = x.ProductId,
                    ProductPrice = x.Product.Price,
                    ProductTitle = x.Product.Title,
                    Quantity = x.Quantity,
                    ProductCurrency = x.Product.Currency
                }).ToList()
            };
        }
    }
}
