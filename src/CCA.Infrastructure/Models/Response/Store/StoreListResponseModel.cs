﻿using System;
using CCA.Infrastructure.Models.Response.Common;

namespace CCA.Infrastructure.Models.Response.Store
{
    public class StoreListResponseModel : PagedListModel<StoreListModelItem>
    {
        
    }

    public class StoreListModelItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
