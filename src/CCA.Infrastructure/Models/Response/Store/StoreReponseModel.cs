﻿using System;
namespace CCA.Infrastructure.Models.Response.Store
{
    public class StoreReponseModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String PhoneNumber { get; set; }
    }
}
