﻿using System;
using System.Collections.Generic;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Response.ProductItem;

namespace CCA.Infrastructure.Models.Response.Basket
{
    public class BasketResponseModel
    {
        public string BasketNumber { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ModifiedAt { get; set; }

        public bool Active { get; set; }

        public double TotalPrice { get; set; }

        public List<ProductItemResponseModel> ProductItems { get; set; }
    }
}
