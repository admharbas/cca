﻿using System;
using CCA.Core.Entities;

namespace CCA.Infrastructure.Models.Response.Token
{
    public class TokenResponseModel
    {
        public String Id { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
