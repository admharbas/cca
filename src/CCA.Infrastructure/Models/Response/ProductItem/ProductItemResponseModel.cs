﻿using System;
namespace CCA.Infrastructure.Models.Response.ProductItem
{
    public class ProductItemResponseModel
    {
        public int Id { get; set; }

        public int Quantity { get; set; }

        public int ProductId { get; set; }

        public string ProductTitle { get; set; }

        public double ProductPrice { get; set; }

        public string ProductCurrency { get; set; }
    }
}
