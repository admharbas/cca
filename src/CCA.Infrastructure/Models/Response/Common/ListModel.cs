﻿using System;
using System.Collections.Generic;

namespace CCA.Infrastructure.Models.Response.Common
{
    public class ListModel<T>
    {
        public ListModel()
        {
        }

        public ListModel(IEnumerable<T> items)
        {
            this.Items = items;
        }

        public IEnumerable<T> Items { get; set; }
    }
}
