﻿using System;
namespace CCA.Infrastructure.Models.Response.Common
{
    public class PagedListModel<T> : ListModel<T>
    {
        public PagedListModel()
        {
            Page = 1;
            All = false;
        }

        public int Page { get; set; }

        public int Total { get; set; }

        public bool All { get; set; }
    }
}
