﻿using System;
namespace CCA.Infrastructure.Models.Response.Customer
{
    public class CustomerResponseModel
    {
        public int Id { get; set; }
        public string CustomerNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
    }
}
