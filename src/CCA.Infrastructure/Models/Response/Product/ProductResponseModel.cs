﻿using System;
namespace CCA.Infrastructure.Models.Response.Product
{
    public class ProductResponseModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
