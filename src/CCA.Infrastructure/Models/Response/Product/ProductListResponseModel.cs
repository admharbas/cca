﻿using System;
using CCA.Infrastructure.Models.Response.Common;

namespace CCA.Infrastructure.Models.Response.Product
{
    public class ProductListResponseModel : PagedListModel<ProductListModelItem>
    {

    }

    public class ProductListModelItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public double VAT { get; set; }
        public string Currency { get; set; }
        public int InStock { get; set; }
    }
}
