﻿using System;
using CCA.Infrastructure.Models.Request.Common;

namespace CCA.Infrastructure.Models.Request.Store
{
    /*
        Store list request model. Wrapper for request stores.
    */
    /// <summary>
    /// Store list request model. Wrapper for request stores.
    /// </summary>
    public class StoreListRequestModel : PagedRequestModel
    {
        public String Filter { get; set; }
    }
}
