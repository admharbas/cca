﻿using System;
namespace CCA.Infrastructure.Models.Request.ProductItem
{
    public class ProductItemRequestModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
