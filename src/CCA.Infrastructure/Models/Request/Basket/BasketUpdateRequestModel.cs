﻿using System;
using System.Collections.Generic;
using CCA.Core.Entities;
using CCA.Infrastructure.Models.Request.ProductItem;

namespace CCA.Infrastructure.Models.Request.Basket
{
    public class BasketUpdateRequestModel
    {
        public List<ProductItemRequestModel> ProductItems { get; set; }
    }
}
