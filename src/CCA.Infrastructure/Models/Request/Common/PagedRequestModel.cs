﻿using System.ComponentModel.DataAnnotations;

namespace CCA.Infrastructure.Models.Request.Common
{
    /*
        Paged request model.
        It is a wrapper for pagination details for lists of object.
    */
    /// <summary>
    ///  Paged request model.
    ///  It is a wrapper for pagination details for lists of object.
    /// </summary>
    public class PagedRequestModel
    {
        public PagedRequestModel()
        {
            Page = 1;
            Count = 16;
            All = false;
        }

        [Range(1,1000)]
        public int Page { get; set; }

        [Range(1, 100)]
        public int Count { get; set; }

        public bool All { get; set; }
    }
}
