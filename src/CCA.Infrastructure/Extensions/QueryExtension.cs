﻿using System;
using System.Linq;
using CCA.Infrastructure.Models.Request.Common;

namespace CCA.Infrastructure.Extensions
{
    public static class QueryExtension
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> source, PagedRequestModel model)
        {
            return model.All ? source
                             : source.Skip((model.Page - 1) * model.Count)
                                     .Take(model.Count);
        }
    }
}
