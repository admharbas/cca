﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCA.Core.Entities
{
    /*
        The Base Entity class.
        Contains all shared properties and every entity should use it as base class.
    */
    /// <summary>
    /// The Base Entity class.
    /// Contains all shared properties and every entity should use it as base class.
    /// </summary>
    public class BaseEntity
    {
        /// <value>Autro generated id for entity.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        /// <value>Date and time when record is created.</value>
        [Required]
        public DateTime CreatedAt { get; set; }

        /// <value>Date and time when record is updated.</value>
        [Required]
        public DateTime ModifiedAt { get; set; }
    }
}
