﻿using System;
using System.Collections.Generic;

namespace CCA.Core.Entities
{
    /*
        Product entity class.
    */
    /// <summary>
    /// Product entity class.
    /// </summary>
    public class Product : BaseEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string Currency { get; set; }

        public double VAT { get; set; }

        public int InStock { get; set; }

        public bool Active { get; set; }

        public int StoreId { get; set; }
        public virtual Store Store { get; set; }

        public virtual ICollection<ProductItem> ProductItems { get; set; }
    }
}
