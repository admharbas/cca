﻿using System;
using System.Collections.Generic;

namespace CCA.Core.Entities
{
    public class Order : BaseEntity
    {
        public string OrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public double TotalPrice { get; set; }

        public bool Payed { get; set; }

        public int CustomerId { get; set; }
        public virtual User Customer { get; set; }

        public int BasketId { get; set; }
        public virtual Basket Basket { get; set; }

        public virtual ICollection<ProductItem> ProductItems { get; set; }
    }
}
