﻿using System.ComponentModel.DataAnnotations;

namespace CCA.Core.Entities
{
    /*
        Store entity class.
    */
    /// <summary>
    /// Store entity class.
    /// </summary>
    public class Store : BaseEntity
    {
        /// <value>Store name.</value>
        [Required]
        public string Name { get; set; }

        /// <value>Store phone number.</value>
        public string PhoneNumber { get; set; }
    }
}
