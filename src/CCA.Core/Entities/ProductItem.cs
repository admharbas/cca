﻿using System;
namespace CCA.Core.Entities
{
    public class ProductItem : BaseEntity
    {
        public int Quantity { get; set; }

        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

        public int BasketId { get; set; }
        public virtual Basket Basket { get; set; }

        public bool Active { get; set; }

        public int? OrderId { get; set; }
        public virtual Order Order { get; set; }
    }
}
