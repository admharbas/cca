﻿using System;
using System.Collections.Generic;

namespace CCA.Core.Entities
{
    public class Basket : BaseEntity
    {
        public string BasketNumber { get; set; }

        public bool Active { get; set; }

        public double TotalPrice { get; set; }

        public int CustomerId { get; set; }
        public virtual User Customer { get; set; }

        public virtual ICollection<ProductItem> ProductItems { get; set; }
    }
}
