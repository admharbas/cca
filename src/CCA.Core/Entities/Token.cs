﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCA.Core.Entities
{
    public class Token
    {
        [Key, Column(Order = 0)]
        public String Id { get; set; }

        [Required]
        public String OwnerCustomerNumber { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int CustomerId { get; set; }
        public virtual User Customer { get; set; }
    }
}
